# WasherApp-in-Java



import javax.swing.JOptionPane;

public class WasherApp{

  
  // postcondition: returns a type double data value
  private static double readDouble(String prompt){
  String numStr = JOptionPane.showInputDialog(prompt);
  return Double.parseDouble(numStr);
 }
 
 // postcondition: returns a type int data value
  private static int readInt(String prompt) {
  String numStr = JOption.showInputDialog(prompt);
  return Integer.parseInt(numStr);
 }  
  
  public static void main(String[] args){
  // Create a Washer object
  Washer wash = new Washer();
  
  //Get the washer data and store it in the Washer object.
  wash.setInner(readDouble("Enter inner diameter in centimeters") / 2);
  wash.setOuter(readDouble("Enter outer diameter in centimeters") / 2);
  wash.setThickness(readDouble("Enter thickness in centimeters") / 2);
  wash.setDensity(readDouble("Enter density in grams per cc"));
  
  // Get the quantity of washers.
  int quantity = readInt("Enter quantity");
  
  // Calculate the batch weight
  double batchWeight = quantity * wash.computeWeight();
  
  //Display the washer information and the batch weight.
  JOptionPane.showMessageDialog(null, wash.toString() + "\nThe weight of " + quantity + "washers is" + batchWeight + "grams");
 }
}   
